package com.wisemapping.dao;

import com.wisemapping.model.Label;
import com.wisemapping.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.util.List;

public class LabelManagerImpl extends HibernateDaoSupport
        implements LabelManager {

    @Override
    public void addLabel(@NotNull final Label label) {
        saveLabel(label);
    }

    @Override
    public void saveLabel(@NotNull final Label label) {
        currentSession().save(label);
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<Label> getAllLabels(@NotNull final User user) {
        var query = currentSession().createQuery("from com.wisemapping.model.Label wisemapping where creator_id=:creatorId");
        query.setParameter("creatorId", user.getId());
        return query.list();
    }

    @Nullable
    @Override
    public Label getLabelById(int id, @NotNull final User user) {
        var query = currentSession().createQuery("from com.wisemapping.model.Label wisemapping where id=:id and creator=:creator");
        query.setParameter("id", id);
        query.setParameter("creator", user);
        return getFirst(query.list());
    }

    @Nullable
    @Override
    public Label getLabelByTitle(@NotNull String title, @NotNull final User user) {
        var query = currentSession().createQuery("from com.wisemapping.model.Label wisemapping where title=:title and creator=:creator");
        query.setParameter("title", title);
        query.setParameter("creator", user);
        return getFirst(query.list());
    }

    @Override
    public void removeLabel(@NotNull Label label) {
        getHibernateTemplate().delete(label);
    }

    @Nullable private Label getFirst(List<Label> labels) {
        Label result = null;
        if (labels != null && !labels.isEmpty()) {
            result = labels.get(0);
        }
        return result;
    }

}
